/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.Files;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author nati2
 */
public class AdminLogic {
    
    Files files = new Files();

    public boolean compareAirports(String departureAirport , String arrivalAirport){
        boolean result = false;
        if(!departureAirport.equals(arrivalAirport)){
            result = true;
        }
        return result;
    }
    
    public boolean compareDates(Date departureDate , Date arrivalDate){
        boolean result = false;
        if(departureDate.before(arrivalDate)){
            result = true;
        }
        return result;
    }
    
    public String getDifferenceBetweenHours(Date departureHour , Date arrivalHour){
        long diff = arrivalHour.getTime() - departureHour.getTime();
        long hours = TimeUnit.MILLISECONDS.toHours(diff); 
        
        long spentHoursToMinutes = TimeUnit.HOURS.toMinutes(hours);        
        long minutesLeft = TimeUnit.MILLISECONDS.toMinutes(diff) - spentHoursToMinutes;
        
        String parsedHours = Long.toString(hours);
        String parsedMinutes = Long.toString(minutesLeft);
        
        return parsedHours + ":" + parsedMinutes;
    }
    
    public String[] checkAvailability(int airlineIndex){
        return files.checkAvailability(airlineIndex);
    }
    
    public String getEntityIdFromPlanes(int airlineIndex){
        return files.getEntityIdFromPlanes(airlineIndex);
    }
    
    public String getEntityIdFromAirCrew(int airlineIndex, String employeeRoleValue){
        return files.getEntityIdFromAirCrew(airlineIndex, employeeRoleValue);
    }  
}
