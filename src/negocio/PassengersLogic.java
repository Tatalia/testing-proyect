/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;


/**
 *
 * @author nati2
 */
/*
1,01-O,02-O,03-O,04-O,05-O,06-O,07-O,08-O,09-O,10-O,11-O,12-O,13-O,14-O,15-O,16-O,17-O,18-O,19-O,20-O,21-O,22-O,23-O,24-O,25-O,26-O,27-O,28-O,29-O,30-T,31-O,32-O,33-O,34-T,35-T,36-T 
//Respuesta: No hay suficientes campos disponibles

//Familia de 5 con solo 3 campos disponibles en ala A juntos
2,01-O,02-O,03-O,04-O,05-O,06-O,07-A,08-C,09-P,10-O,11-O,12-O,13-O,14-O,15-O,16-O,17-O,18-O,19-O,20-O,21-O,22-O,23-O,24-O,25-O,26-O,27-O,28-O,29-O,30-T,31-O,32-O,33-O,34-T,35-T,36-T 
//Respuesta: No hay suficientes campos disponibles

//campo 01 ocupado. Una persona compra ticket 
3,01-O,02-C,03-P,04-P,05-C,06-A,07-A,08-C,09-P,10-P,11-C,12-A,13-A,14-C,15-P,16-P,17-C,18-A,19-A,20-C,21-P,22-P,23-C,24-A,25-A,26-C,27-P,28-P,29-C,30-T,31-A,32-C,33-P,34-T,35-T,36-T
//Respuesta: Person es asignada a campo 3

//campo 01 ocupado. Familia(3) compra ticket
4,01-O,02-C,03-P,04-P,05-C,06-A,07-A,08-C,09-P,10-P,11-C,12-A,13-A,14-C,15-P,16-P,17-C,18-A,19-A,20-C,21-P,22-P,23-C,24-A,25-A,26-C,27-P,28-P,29-C,30-T,31-A,32-C,33-P,34-T,35-T,36-T
//Respuesta: familia es asignada a 03,04,05

//01 y 02 ocupados. Pasajero compra ticket
5,01-O,02-O,03-P,04-P,05-C,06-A,07-A,08-C,09-P,10-P,11-C,12-A,13-A,14-C,15-P,16-P,17-C,18-A,19-A,20-C,21-P,22-P,23-C,24-A,25-A,26-C,27-P,28-P,29-C,30-T,31-A,32-C,33-P,34-T,35-T,36-T
//Respuesta: pasajero es asignado a 04 y 03 se vuelve(X)

//01,02,03 asignados, pasajero compra ticket
6,01-O,02-O,03-O,04-P,05-C,06-A,07-A,08-C,09-P,10-P,11-C,12-A,13-A,14-C,15-P,16-P,17-C,18-A,19-A,20-C,21-P,22-P,23-C,24-A,25-A,26-C,27-P,28-P,29-C,30-T,31-A,32-C,33-P,34-T,35-T,36-T
//Respuesta: campo 4 es asignado debido a la separacion de pasillo
*/
public class PassengersLogic {

    public String updateFlightSeats(int indexOfFlights, int numberOfPassengers) {
        File entityFile = new File("seats.txt");
        String result = "";
        boolean thereAreAvailableSeats = false;
        String singleEntityToPass = "";
        String indexOfSeats = "";
        
        String tempIndex = "04-Z";//For testing only

        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            for (String singleEntity : entityArray) {//divide el documento en lineas
                String[] tempArray = singleEntity.split(",", 0);//divide la linea por comas
                if(tempArray[0].equals(tempIndex)){//vuelo encontrado
                    int val = 1;
                    String[] updatedEntity = getUpdatedEntity(tempArray , numberOfPassengers);
                    int val2 = 1;
                    /*
                    for (String seat : tempArray){
                        String[] seatIndividualArray = seat.split("-", 0); 
                        
                        if(!seatIndividualArray[1].replace(" ", "").equals("O") && 
                            !seatIndividualArray[1].replace(" ", "").equals("Z") && 
                            !seatIndividualArray[1].replace(" ", "").equals("X") && 
                            !seatIndividualArray[1].replace(" ", "").equals("T")){//Campos disponibles
                            thereAreAvailableSeats = true;
                            singleEntityToPass = singleEntity;
                            indexOfSeats = tempArray[0];
                        }
                    } */

                }
            }
            entity.close();//cerramos el reader para poder implementar el edit
            if(thereAreAvailableSeats){
                result = updateSeatFile(entityArray, singleEntityToPass, indexOfSeats);
            }else{
                result = "No hay asientos disponibles :(";
            }        
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: aircrew.txt\n" + ex);
        }
        return result;
    }
    
    public String[] getUpdatedEntity(String[] singleEntityArray, int numberOfPassengers){
        boolean thereIsSomeoneInTheFliht = false;
        boolean wasAlreadySeparated = false;    
        String[] cloneArray = singleEntityArray;
        
        //(int i = 0; i < 5; i++)
     //for (int i = 1; i <= parsedMonths; i++) {
        for(int i = 1; i <= singleEntityArray.length; i++ ){
            String seat = singleEntityArray[i];
            String[] seatIndividualArray = seat.split("-", 0); 
            if(seatIndividualArray[1].replace(" ", "").equals("O")){
                thereIsSomeoneInTheFliht = true;
                i = singleEntityArray.length;
            }           
        }

        for(int u = 1; u <= numberOfPassengers; u++){//itera por cant de pasajeros
            for(int i = 0; i <= singleEntityArray.length; i++ ){//recorro arreglo de asientos por asientos disponibles
                String seat = singleEntityArray[i];
                String[] seatIndividualArray = seat.split("-", 0); 
                if(!seatIndividualArray[1].replace(" ", "").equals("O") && 
                    !seatIndividualArray[1].replace(" ", "").equals("Z") && 
                    !seatIndividualArray[1].replace(" ", "").equals("X") && 
                    !seatIndividualArray[1].replace(" ", "").equals("T")){
                    
                    if(thereIsSomeoneInTheFliht == true && wasAlreadySeparated == false){
                        singleEntityArray[i] = seatIndividualArray[0] + "-X";
                        cloneArray[i] = seatIndividualArray[0] + "-X";
                        wasAlreadySeparated = true;
                    }else{
                        singleEntityArray[i] = seatIndividualArray[0] + "-O";//encuentro asiento y lo pongo en ocupado
                        cloneArray[i] = seatIndividualArray[0] + "-O";
                        i = singleEntityArray.length;
                    }                   
                }           
            }
        }
        
        return cloneArray;
    }
    
    public String updateSeatFile(ArrayList<String> entityArray, String singleEntity, String index){
        String result = "";
        File entityFile = new File("seats.txt");
        String filePath = entityFile.getAbsolutePath();

        try {
            Files.delete(Paths.get(filePath));
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n");
            result = "Error al insertar información al archivo";
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s not empty%n", filePath);
            result = "Error al insertar información al archivo";
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
            result = "Error al insertar información al archivo";
        }

        try { 
            entityFile.createNewFile();
        } catch (Exception ex) {
            System.out.println("Error al crear el archivo:\n" + ex);
            result = "Error al insertar información al archivo";
        }

        for (String flight : entityArray) {
            String[] tempArray = flight.split(",", 0);
            if(tempArray[0].equals(index)){
                try {
                    BufferedWriter jobs = new BufferedWriter(new FileWriter(entityFile, true));
                    jobs.write(singleEntity + "\r\n");
                    jobs.close();
                    result = "Asientos Asignados satisfactoriamente!!";
                } catch (Exception ex) {
                    System.out.println("Error al insertar información al archivo:\n" + ex);
                    result = "Error al insertar información al archivo";
                }
            }else{  
                try {
                    BufferedWriter jobs = new BufferedWriter(new FileWriter(entityFile, true));
                    jobs.write(flight + "\r\n");
                    jobs.close();
                    result = "Asientos Asignados satisfactoriamente!!";
                } catch (Exception ex) {
                    System.out.println("Error al insertar información al archivo:\n" + ex);
                    result = "Error al insertar información al archivo";
                }
            } 
        }                     
        return result;
    }
    
    
}
